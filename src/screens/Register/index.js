import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Modal, StatusBar, TextInput, TouchableOpacity } from 'react-native';
import styles from '../../style';
import Tombol from '../../components/Tombol';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';


function Register({ navigation }) {
	let camera
	const [isVisible, setIsVisible ] = useState(false)
	const [type, setType ] = useState('back')
	const [photo, setPhoto ] = useState(null)

	useEffect(() => {
		//diperlukan untuk upload ke firebase storage tanpa login terlebih dahulu (tahap register)
		auth().signInAnonymously()
	}, [])

	const toggleCamera = () => {
		setType(type === 'back' ? 'front' : 'back')
	}

	const takePicture = async () => {
		const options = { quality: 0.5, base64: true }
		if(camera) {
			const data = await camera.takePictureAsync(options)
			setPhoto(data)
			setIsVisible(false)
			// console.log('takePicture -> data', data)
		}
	}

	const uploadImage = (uri) => {
		const sessionId = new Date().getTime()
		return storage()
		.ref(`images/${sessionId}`)
		.putFile(uri)
		.then((response) => {
			alert('Upload Success')
		})
		.catch((error) => {
			alert(error)
		})
	}

	const renderCamera = () => {
		return (
			<Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
				<View style={{flex: 1}}>
					<RNCamera
						style={{flex: 1, justifyContent: 'space-around', flexDirection: 'column'}}
						ref={ref => {camera = ref;}}
						type={type}
					>
						<View style={styles.btnContainer}>
							<TouchableOpacity style={styles.btnFlip} onPress={() => toggleCamera()}>
								<MaterialCommunity name="rotate-3d-variant" size={15} />
							</TouchableOpacity>
						</View>
						<View style={styles.round} />
						<View style={styles.rectangle} />
						<View style={styles.btnContainer}>
							<TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
								<Icon name="camera" size={30} />
							</TouchableOpacity>
						</View>
					</RNCamera>
				</View>
			</Modal>
		)
	}

	return (
		<View style={styles.container}>
			{renderCamera()}
			<View style={styles.header}>
				<Image
					style={styles.profilePicture}
					source={photo === null ? require('../../assets/images/profile_picture.png') : { uri: photo.uri }}
				/>
				<TouchableOpacity onPress={() => setIsVisible(true)}>
					<Text style={styles.userName}>Change Picture</Text>
				</TouchableOpacity>
			</View>
			<View style={[styles.body,{height: 350}]}>
				<View style={styles.line}>
					<Text style={[styles.formLabel,{fontWeight: 'bold'}]}>Nama</Text>
                    <TextInput
                        placeholder='Nama'
                        style={styles.textInput}
                    />
				</View>
				<View style={styles.line}>
					<Text style={[styles.formLabel,{fontWeight: 'bold'}]}>Email</Text>
                    <TextInput
                        placeholder='Email'
                        style={styles.textInput}
                    />
				</View>
				<View style={styles.line}>
					<Text style={[styles.formLabel,{fontWeight: 'bold'}]}>Password</Text>
                    <TextInput
                        placeholder='Password'
                        style={styles.textInput}
                    />
				</View>
				<View style={styles.line}>
					<Tombol bgColor={'#3ec6ff'} onPress={() => uploadImage(photo.uri)} title={"Register"} />
				</View>
			</View>
		</View>
	)
}

export default Register;