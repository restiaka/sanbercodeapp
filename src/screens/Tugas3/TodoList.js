import React, { useState } from 'react';
import { StyleSheet, StatusBar, View, Text, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/AntDesign';

const TodoList = () => {
	const [ input, setInput ] = useState('')
	const [ todos, setTodos ] = useState([])
	const [ refresh, setRefresh ] = useState(true)

	const renderItem = ({item,index}) => {
		return (
			<View style={styles.list}>
				<View>
					<Text>{item.date}</Text>
					<Text>{item.title}</Text>
				</View>
				<TouchableOpacity style={styles.deleteButton} onPress={() => deleteTodo(index)}>
					<Icon name="delete" size={25} />
				</TouchableOpacity>
			</View>
		)
	}

	return (
		<View style={styles.container}>
			<View style={styles.formContainer}>
				<Text style={{marginBottom: 10}}>Masukkan To Do List</Text>
				<View style={styles.row}>
					<TextInput
						style={styles.input}
						value={input}
						placeholder="Input Here"
						onChangeText={(todo) => setInput(todo)}
					/>
					<TouchableOpacity style={styles.addButton} onPress={() => addTodo()}>
						<Ionicons name="add" size={25} />
					</TouchableOpacity>
				</View>
			</View>
			<View style={styles.listContainer}>
				<FlatList
					data={todos}
					renderItem={renderItem}
					keyExtractor={(item,index)=>index.toString()}
					extraData={refresh}
				/>
			</View>
		</View>
	)

	function addTodo() {
		if (input) {
			var d = new Date();
			var now = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
			setTodos([...todos, {title: input, date: now}])
			setInput('')
		}
	}

	function deleteTodo(index) {
		todos.splice(index, 1)
		/*state refresh digunakan di prop extraData di flatlist untuk memberitahu komponen flatlist agar mereload data dari state todos terbaru*/
		setRefresh(!refresh)
	}

}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	formContainer: {
		marginTop: 10,
		padding: 10,
		height: 100
	},
	row: {
		flex: 1,
		flexDirection: 'row'
	},
	input: {
		flex: 1,
		height: 50,
		alignItems: 'stretch',
	    borderWidth: 1,
	    borderColor: '#cacaca',
	    color: '#000',
	    paddingHorizontal: 20
	},
	addButton: {
		height: 50,
		width: 50,
	    backgroundColor: '#3ec6ff',
	    alignItems: 'center',
	    justifyContent: 'center',
	    marginLeft: 5
	},
	listContainer: {
		flex: 1,
		paddingHorizontal: 10
	},
	list: {
		padding: 20,
	    paddingRight: 100,
	    borderWidth: 5,
	    borderRadius: 5,
	    borderColor: '#ededed',
	    marginBottom: 10
	},
	deleteButton: {
		position: 'absolute',
	    justifyContent: 'center',
	    alignItems: 'center',
	    padding: 10,
	    top: 10,
	    bottom: 10,
	    right: 10
	}
});

export default TodoList;