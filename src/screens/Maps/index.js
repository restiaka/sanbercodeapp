import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken('pk.eyJ1IjoicmVzdGlha2EiLCJhIjoiY2tkZmU4eDFmMWN3cjJ5bXQ5ZzNsc2EwNSJ9.0p7AhCCxDKUxqwN5bqtFfg')

const Maps = () => {
	const [ coordinates, setCoordinates ] = useState([
						    [107.598827, -6.896191],
						    [107.596198, -6.899688],
						    [107.618767, -6.902226],
						    [107.621095, -6.898690],
						    [107.615698, -6.896741],
						    [107.613544, -6.897713],
						    [107.613697, -6.893795],
						    [107.610714, -6.891356],
						    [107.605468, -6.893124],
						    [107.609180, -6.898013]
						])

	useEffect(() => {
		const getLocation = async() => {
			try {
				const permission = await MapboxGL.requestAndroidLocationPermissions()
			} catch(err) {
				console.log(err)
			}
		}
		getLocation()
	}, [])

	const Markers = () => {
		return coordinates.map((data, i) => {
			let [long,lat] = data
			return (
				<MapboxGL.PointAnnotation id={"pointAnnotation"+i} coordinate={data} key={i} >
					<MapboxGL.Callout title={"Longitude : "+long+" Latitude : "+lat}  key={i} />
				</MapboxGL.PointAnnotation>
			)
		})
	}

	return (
		<MapboxGL.MapView
			style={{flex:1}}
		>
			<MapboxGL.UserLocation
				visible={true}
			/>
			<MapboxGL.Camera
				followUserLocation={true}
			/>
			<Markers />
		</MapboxGL.MapView>
	)
}

export default Maps;