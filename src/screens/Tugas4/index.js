import React, { useState, createContext } from 'react';
import TodoList from './TodoList';

export const RootContext = createContext();

const Context = () => {

	const [ input, setInput ] = useState('')
	const [ todos, setTodos ] = useState([])
	const [ refresh, setRefresh ] = useState(true)

	const handleChangeInput = (value) => {
		setInput(value)
	}

	const addTodo = () => {
		var d = new Date();
		var now = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
		setTodos([...todos, {title: input, date: now}])
		setInput('')
	}

	const deleteTodo = (index) => {
		todos.splice(index, 1)
		/*state refresh digunakan di prop extraData di flatlist untuk memberitahu komponen flatlist agar mereload data dari state todos terbaru*/
		setRefresh(!refresh)
	}

	return (
		<RootContext.Provider value={{ input, todos, handleChangeInput, addTodo, deleteTodo, refresh }}>
			<TodoList />
		</RootContext.Provider>
	)
}

export default Context;