import React, { useState } from 'react';
import { Text, View, processColor } from 'react-native';
import styles from '../../style';
import colors from '../../style/colors';
import { BarChart } from 'react-native-charts-wrapper';

const ReactNative = ({navigation}) => {
	const data = [
	    {y:[100, 40], marker: ["React Native Dasar", "React Native Lanjutan"]},
	    {y:[80, 60], marker: ["React Native Dasar", "React Native Lanjutan"]},
	    {y:[40, 90], marker: ["React Native Dasar", "React Native Lanjutan"]},
	    {y:[78, 45], marker: ["React Native Dasar", "React Native Lanjutan"]},
	    {y:[67, 87], marker: ["React Native Dasar", "React Native Lanjutan"]},
	    {y:[98, 32], marker: ["React Native Dasar", "React Native Lanjutan"]},
	    {y:[150, 90], marker: ["React Native Dasar", "React Native Lanjutan"]},
	]

	const newData = Array.from(data, function(item){
		const [a,b] = item.y
		const [a1,b1] = item.marker
		return {y:item.y,marker:[`${a1}\n${a}`,`${b1}\n${b}`]}
	})

	const [legend, setLegend] = useState({
	    enabled: true,
	    textSize: 14,
	    form: 'SQUARE',
	    formSize: 14,
	    xEntrySpace: 10,
	    yEntrySpace: 5,
	    formToTextSpace: 5,
	    wordWrapEnabled: true,
	    maxSizePercent: 0.5
	})
	const [chart, setChart] = useState({
	    data: {
	        dataSets: [{
	            values: newData,
	            label: '',
	            config: {
	                colors: [processColor(colors.blue), processColor(colors.darkblue)],
	                stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
	                drawFilled: false,
	                drawValues: false,
	            }
	        }]
	    }
	})
	const [xAxis, setXAxis] = useState({
	    valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
	    position: 'BOTTOM',
	    drawAxisLine: true,
	    drawGridLines: false,
	    axisMinimum: -0.5,
	    granularityEnabled: true,
	    granularity: 1,
	    axisMaximum: new Date().getMonth() + 0.5,
	    spaceBetweenLabels: 0,
	    labelRotationAngle: -45.0,
	    limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
	})
	const [yAxis, setYAxis] = useState({
	    left: {
	        axisMinimum: 0,
	        labelCountForce: true,
	        granularity: 5,
	        granularityEnabled: true,
	        drawGridLines: false
	    },
	    right: {
	        axisMinimum: 0,
	        labelCountForce: true,
	        granularity: 5,
	        granularityEnabled: true,
	        enabled: false
	    }
	})

	return (
		<View style={styles.container}>
			<BarChart
				style={{flex:1}}
				data={chart.data}
				yAxis={yAxis}
				xAxis={xAxis}
				doubleTapToZoomEnabled={false}
				chartDescription={{ text: '' }}
				legend={legend}
				marker={{
					enabled: true,
					markerColor: processColor('grey'),
					textColor: processColor('white'),
					textSize: 14
				}}
			/>
		</View>
	)
}

export default ReactNative;