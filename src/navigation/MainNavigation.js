import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Intro from '../screens/Intro';
import Login from '../screens/Login';
import Register from '../screens/Register';
import Dashboard from './Dashboard';
import ReactNativeScreen from '../screens/ReactNative';

const Stack = createStackNavigator();

const MainNavigation = (props) => (
    <Stack.Navigator initialRouteName={props.isLogin === 'true' && 'Dashboard'}>
    	{ props.intro == null && <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} /> }
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Dashboard" component={Dashboard} options={{ headerShown: false }} />
        <Stack.Screen name='ReactNative' component={ReactNativeScreen} />
        <Stack.Screen name='Register' component={Register} />
    </Stack.Navigator>
)

export default MainNavigation;