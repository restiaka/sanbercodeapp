import React, { useContext } from 'react';
import { StyleSheet, StatusBar, View, Text, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/AntDesign';
import { RootContext } from '../Tugas4'

const TodoList = () => {
	const state = useContext(RootContext)

	const renderItem = ({item,index}) => {
		return (
			<View style={styles.list}>
				<View>
					<Text>{item.date}</Text>
					<Text>{item.title}</Text>
				</View>
				<TouchableOpacity style={styles.deleteButton} onPress={() => state.deleteTodo(index)}>
					<Icon name="delete" size={25} />
				</TouchableOpacity>
			</View>
		)
	}

	return (
		<View style={styles.container}>
			<StatusBar backgroundColor="#3ec6ff" />
			<View style={styles.formContainer}>
				<Text style={{marginBottom: 10}}>Masukkan To Do List</Text>
				<View style={styles.row}>
					<TextInput
						style={styles.input}
						value={state.input}
						placeholder="Input Here"
						onChangeText={(todo) => state.handleChangeInput(todo)}
					/>
					<TouchableOpacity style={styles.addButton} onPress={() => state.addTodo()}>
						<Ionicons name="add" size={25} />
					</TouchableOpacity>
				</View>
			</View>
			<View style={styles.listContainer}>
				<FlatList
					data={state.todos}
					renderItem={renderItem}
					keyExtractor={(item,index)=>index.toString()}
					extraData={state.refresh}
				/>
			</View>
		</View>
	)

}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	formContainer: {
		marginTop: 10,
		padding: 10,
		height: 100
	},
	row: {
		flex: 1,
		flexDirection: 'row'
	},
	input: {
		flex: 1,
		height: 50,
		alignItems: 'stretch',
	    borderWidth: 1,
	    borderColor: '#cacaca',
	    color: '#000',
	    paddingHorizontal: 20
	},
	addButton: {
		height: 50,
		width: 50,
	    backgroundColor: '#3ec6ff',
	    alignItems: 'center',
	    justifyContent: 'center',
	    marginLeft: 5
	},
	listContainer: {
		flex: 1,
		paddingHorizontal: 10
	},
	list: {
		padding: 20,
	    paddingRight: 100,
	    borderWidth: 5,
	    borderRadius: 5,
	    borderColor: '#ededed',
	    marginBottom: 10
	},
	deleteButton: {
		position: 'absolute',
	    justifyContent: 'center',
	    alignItems: 'center',
	    padding: 10,
	    top: 10,
	    bottom: 10,
	    right: 10
	}
});

export default TodoList;