import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MainNav from './MainNavigation';
import SplashScreen from '../screens/SplashScreen';
import AsyncStorage from '@react-native-community/async-storage';

function AppNavigation() {
    const [ isLoading, setIsLoading ] = React.useState(true)
    const [ token, setToken ] = React.useState(null)
    const [ intro, setIntro ] = React.useState(null)
    const [ isLogin, setIsLogin ] = React.useState('false')

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

        async function getStatus() {
            const skipped = await AsyncStorage.getItem('skipped')
            setIntro(skipped)
        }

        async function getLoginStatus() {
            const isLogin = await AsyncStorage.getItem("isLogin")
            setIsLogin(isLogin)
        }

        getLoginStatus()
        getStatus()
    }, [])

    if(isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNav intro={intro} isLogin={isLogin} />
        </NavigationContainer>
    )
}

export default AppNavigation;