import React, { useState, useEffect } from 'react';
import { View, Image, Text, StyleSheet, Button } from 'react-native';
import Tombol from '../../components/Tombol';
import Axios from 'axios';
import api from '../../api';
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'

function Biodata({ navigation }) {

	const [ userInfo, setUserInfo ] = useState(null)

	useEffect(() => {
		async function getToken() {
			try {
				const token = await AsyncStorage.getItem("token")
				if(token){
					return getVenue(token)
				}
				console.log("getToken -> token", token)
			} catch(err) {
				console.log(err)
			}
		}
		getToken()
		getCurrentUser()
	}, [])

	const getCurrentUser = async () => {
		try {
			const userInfo = await GoogleSignin.signInSilently()
			setUserInfo(userInfo)
			console.log("userInfo -> ",userInfo);
		} catch(err) {
			console.log(err)
		}
	}

	const getVenue = (token) => {
		Axios.get(`${api}/venues`, {
			timeout: 20000,
			headers: {
				'Authorization' : 'Bearer' + token
			}
		})
		.then((res) => {
			console.log('Profile -> res', res)
		})
		.catch((err) => {
			console.log('Profile -> err', err)
		})
	}

	const onLogoutPress = async () => {
		try {
			if(userInfo) {
				await GoogleSignin.revokeAccess()
				await GoogleSignin.signOut()
			}
			await AsyncStorage.removeItem('token')
			navigation.navigate('Login')
		} catch(err) {
			console.log(err)
		}
	}

	return (
		<View style={styles.container}>
			<Header userInfo={userInfo} />
			<Body aksi={onLogoutPress} userInfo={userInfo}  />
		</View>
	)
}

const Header = function divHeader(props) {
	return (
		<View style={styles.header}>
			{(props.userInfo) ?
				<Image style={styles.profilePicture} source={{uri: props.userInfo && props.userInfo.user && props.userInfo.user.photo}} />
				:
				<Image style={styles.profilePicture} source={require('../../assets/images/profile_picture.png')} />
			}
			<Text style={styles.userName}>{(props.userInfo) ? props.userInfo && props.userInfo.user && props.userInfo.user.name : 'Nova Arief Restiaka'}</Text>
		</View>
	)
}

const Body = (props) => {
	return (
		<View style={styles.body}>
			<View style={styles.line}>
				<View style={styles.flex1}>
					<Text style={styles.textLeft}>Tanggal Lahir</Text>
				</View>
				<View style={styles.flex1}>
					<Text style={styles.textRight}>05 November 1987</Text>
				</View>
			</View>
			<View style={styles.line}>
				<View style={styles.flex1}>
					<Text style={styles.textLeft}>Jenis Kelamin</Text>
				</View>
				<View style={styles.flex1}>
					<Text style={styles.textRight}>Laki-Laki</Text>
				</View>
			</View>
			<View style={styles.line}>
				<View style={styles.flex1}>
					<Text style={styles.textLeft}>Hobi</Text>
				</View>
				<View style={styles.flex1}>
					<Text style={styles.textRight}>Gaming</Text>
				</View>
			</View>
			<View style={styles.line}>
				<View style={styles.flex1}>
					<Text style={styles.textLeft}>No. Telp</Text>
				</View>
				<View style={styles.flex1}>
					<Text style={styles.textRight}>081268442780</Text>
				</View>
			</View>
			<View style={styles.line}>
				<View style={styles.flex1}>
					<Text style={styles.textLeft}>Email</Text>
				</View>
				<View style={styles.flex1}>
					<Text style={styles.textRight}>{(props.userInfo) ? props.userInfo && props.userInfo.user && props.userInfo.user.email : 'restiaka@gmail.com'}</Text>
				</View>
			</View>
			<Tombol bgColor={'#3ec6ff'} onPress={props.aksi} title={"Logout"} />
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	header: {
		backgroundColor: '#3ec6ff',
		alignItems: 'center',
		height: 200,
		justifyContent: 'center',
	},
	profilePicture: {
		width: 80,
		height: 80,
		borderRadius: 40,
	},
	userName: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 20,
		paddingTop: 15
	},
	body: {
		marginTop: -25,
		marginHorizontal: 20,
		padding: 20,
		borderRadius: 10,
		backgroundColor: 'white',
		height: 200,
		elevation: 3,
	},
	line: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	textLeft: {
		color: '#000',
		textAlign: 'left',
	},
	textRight: {
		color: '#000',
		textAlign: 'right',
	},
	flex1: {
		flex: 1
	}
});

export default Biodata;