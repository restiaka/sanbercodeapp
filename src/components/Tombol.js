import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';


const Tombol = (props) => {
	return (
		<TouchableOpacity onPress={props.onPress}>
	        <View style={[styles.btnContainer,{backgroundColor: (props.bgColor) ?  props.bgColor : '#3ec6ff'}]}>
	            <Text style={[styles.title,{color: (props.textColor) ?  props.textColor : 'white'}]}>
	                {props.title}
	            </Text>
	        </View>
	    </TouchableOpacity>
	)
}


const styles = StyleSheet.create({
	btnContainer: {
		margin: 4,
		height: 40,
		justifyContent: 'center',
		borderRadius: 3
	},
	title: {
		textAlign: 'center',
		fontWeight: 'bold'
	}
});

export default Tombol;