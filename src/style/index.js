import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white',
	},
	buttonCircle: {
		width: 50,
		height: 50,
		borderRadius: 25,
		borderStyle: 'solid',
		backgroundColor: '#16166c',
		alignItems: 'center',
		justifyContent: 'center',
	},
	slide: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		fontSize: 24,
		color: '#16166c',
		fontWeight: 'bold',
	},
	image: {
		height: 250,
		width: 250,
		marginVertical: 50,
	},
	text: {
		paddingHorizontal: 8,
		fontSize: 15,
		fontWeight: 'bold',
		color: '#a4a7a6',
		textAlign: 'center',
	},
	loginHeader: {
		justifyContent: 'center',
		flexDirection: 'row',
		marginBottom: 30,
	},
	loginBody: {
		paddingHorizontal: 20,
	},
	loginFooter: {
		justifyContent: 'center',
		flexDirection: 'row',
		padding: 20,
	},
	textInput: {
		borderBottomWidth: 2,
		borderBottomColor: '#eee',
		borderStyle: 'solid',
		marginBottom: 20,
	},
	loginPage: {
		flex: 1,
		justifyContent: 'space-between'
	},
	hr: {
		marginVertical: 10,
		height: 1,
		borderWidth: 1,
		borderColor: '#ddd',
		borderStyle: 'solid',
	},
	or: {
		color: '#000',
		alignSelf: 'center',
		textAlign: 'center',
		marginTop: -20,
		marginBottom: 5,
		backgroundColor: '#fff',
		width: 30
	},
	header: {
		backgroundColor: '#3ec6ff',
		alignItems: 'center',
		height: 200,
		justifyContent: 'center',
	},
	profilePicture: {
		width: 80,
		height: 80,
		borderRadius: 40,
	},
	userName: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 20,
		paddingTop: 15
	},
	body: {
		marginTop: -25,
		marginHorizontal: 20,
		padding: 20,
		borderRadius: 10,
		backgroundColor: 'white',
		height: 200,
		elevation: 3,
	},
	btnContainer: {
		justifyContent: 'center',
	},
	btnFlip: {
		backgroundColor: 'white',
		width: 40,
		height: 40,
		borderRadius: 20,
		justifyContent: 'center',
		alignItems: 'center'
	},
	round: {
		alignSelf: 'center',
		width: 200,
		height: 280,
		borderRadius: 150,
		borderWidth: 2,
		borderColor: 'white',
		borderStyle: 'solid',
	},
	rectangle: {
		alignSelf: 'center',
		width: 200,
		height: 100,
		borderWidth: 2,
		borderColor: 'white',
		borderStyle: 'solid',
		marginTop: 40
	},
	btnTake: {
		alignSelf: 'center',
		backgroundColor: 'white',
		width: 70,
		height: 70,
		borderRadius: 35,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 20
	},
})

export default styles