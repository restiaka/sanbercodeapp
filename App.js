import React, { useEffect } from 'react';
// import Intro from './src/screens/Tugas1/Intro';
// import Biodata from './src/screens/Tugas2/Biodata';
// import TodoList from './src/screens/Tugas3/TodoList';
// import Context from './src/screens/Tugas4';
// import Navigation from './src/screens/Tugas5/Navigation';
import Pekan3 from './src/navigation';
import firebase from '@react-native-firebase/app';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';

// // Your web app's Firebase configuration
var firebaseConfig = {
	apiKey: "AIzaSyDn3TeehkmeJBQhWZ-gw0qBKt0PSKyK6KA",
	authDomain: "sanbercode-bdd92.firebaseapp.com",
	databaseURL: "https://sanbercode-bdd92.firebaseio.com",
	projectId: "sanbercode-bdd92",
	storageBucket: "sanbercode-bdd92.appspot.com",
	messagingSenderId: "93109887900",
	appId: "1:93109887900:web:1252d68ba0f8be2189de59",
	measurementId: "G-40V9X4Z94G"
};
// Inisialisasi firebase
if(!firebase.apps.length) {
	firebase.initializeApp(firebaseConfig)
	// firebase.analytics();
}

const App = () => {
	useEffect(() => {
	    OneSignal.setLogLevel(6, 0);

	    OneSignal.init("60651e7e-fe3c-4d0e-a3eb-f42b12194396", {kOSSettingsKeyAutoPrompt : false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption:2});
	    OneSignal.inFocusDisplaying(2);

	    // OneSignal.addEventListener('received', onReceived)
	    // OneSignal.addEventListener('opened', onOpened)
	    // OneSignal.addEventListener('ids', onIds)

	    codePush.sync({
	    	updateDialog: true,
	    	intallMode: codePush.InstallMode.IMMEDIATE
	    }, SyncStatus)

	    // return () => {
	    // 	OneSignal.removeEventListener('received', onReceived)
	    // 	OneSignal.removeEventListener('opened', onOpened)
	    // 	OneSignal.removeEventListener('ids', onIds)
	    // }
	}, [])

	const SyncStatus = (status) => {
		switch (status) {
			case codePush.SyncStatus.CHECKING_FOR_UPDATE:
				console.log('Checking for Update')
				break;
			case codePush.SyncStatus.DOWNLOADING_PACKAGE:
				console.log('Downloading Package')
				break;
			case codePush.SyncStatus.UP_TO_DATE:
				console.log('Up to date')
				break;
			case codePush.SyncStatus.INSTALLING_UPDATE:
				console.log('Installing Update')
				break;
			case codePush.SyncStatus.UPDATE_INSTALLED:
				console.log("Notification", "Update Installed")
				break;
			case codePush.SyncStatus.AWAITING_USER_ACTION:
				console.log('Awaiting User')
				break;
			default:
				break;
		}
	}

    return (
        <Pekan3 />
    )
}

export default App;