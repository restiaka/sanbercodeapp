import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from '../screens/Home';
import Profile from '../screens/Profile';
import Chat from '../screens/Chat';
import MapsScreen from '../screens/Maps';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import EIcons from 'react-native-vector-icons/Entypo';
import FIcons from 'react-native-vector-icons/Feather';
import colors from '../style/colors';

const Tabs = createBottomTabNavigator();

const Dashboard = ({route, navigation}) => {
    return (
        <Tabs.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    if (route.name === 'Home') {
                        return <FIcons name={'home'} size={size} color={color} />;
                    } else if (route.name === 'Profile') {
                        return <FIcons name={'user'} size={size} color={color} />;
                    } else if (route.name === 'Chat') {
                        return <EIcons name={'chat'} size={size} color={color} />;
                    } else if (route.name === 'Maps') {
                        return <EIcons name={'location'} size={size} color={color} />;
                    }
                },
            })}
            tabBarOptions={{
                inactiveBackgroundColor: '#eee',
                activeTintColor: colors.darkblue,
                // inactiveTintColor: '#eee',
            }}
        >
            <Tabs.Screen name="Home" component={Home}/>
            <Tabs.Screen name="Maps" component={MapsScreen} />
            <Tabs.Screen name="Chat" component={Chat} />
            <Tabs.Screen name="Profile" component={Profile} />
        </Tabs.Navigator>
    );
}

export default Dashboard;