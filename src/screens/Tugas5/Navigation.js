import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from './Splashscreen';
import Intro from './Intro';
import Login from './Login';
import Profile from '../Tugas2/Biodata';
import Register from '../Tugas10/Register';

const Stack = createStackNavigator();

const MainNavigation = (props) => (
    <Stack.Navigator>
        { props.intro == null && <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} /> }
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
        <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
    </Stack.Navigator>
)

function AppNavigation() {
    const [ isLoading, setIsLoading ] = React.useState(true)
    const [ token, setToken ] = React.useState(null)
    const [ intro, setIntro ] = React.useState(null)

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

        async function getStatus() {
            const skipped = await AsyncStorage.getItem('skipped')
            setIntro(skipped)
        }
        getStatus()
    }, [])

    if(isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation intro={intro} />
        </NavigationContainer>
    )
}

export default AppNavigation;