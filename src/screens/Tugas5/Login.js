import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import styles from '../../style';
import Tombol from '../../components/Tombol';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api';
import auth from '@react-native-firebase/auth';
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const config = {
    title: 'Authentication Required',
    imageColor: '#191970',
    imageErrorColor: 'red',
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel'
}

function Login({ navigation }) {

    const [ email, setEmail ] = useState('')
    const [ password, setPassword ] = useState('')

    const saveToken = async (token) => {
        try {
            await AsyncStorage.setItem("token", token)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        configureGoogleSignin()
    }, [])

    const configureGoogleSignin = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '93109887900-8ebnbshdplcndhuk87lngcvtbf098pkj.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle = async () => {
        try {
            const { idToken } = await GoogleSignin.signIn()
            console.log("signInWithGoogle -> idToken", idToken)

            const credential = auth.GoogleAuthProvider.credential(idToken)

            auth().signInWithCredential(credential)

            navigation.navigate('Profile')
        } catch (err) {
            // console.log("signInWithGoogle -> err", err)
            ToastAndroid.show('Login failed.\n\n'+err, ToastAndroid.SHORT)
        }
    }

    const signInWithFingerprint = async () => {
        try {
            TouchID.authenticate('', config)
            .then(success => {
                console.log("Authentication Success")
                navigation.navigate('Profile')
            })
            .catch(error => {
                console.log("Authentication Failed")
            })
        } catch (err) {
            console.log("signInWithFingerprint -> err", err)
        }
    }

    // login firebase chat
    const onLoginPress = () => {
        return auth().signInWithEmailAndPassword(email,password)
        .then((res) => {
            navigation.navigate('Dashboard')
        })
        .catch((err) => {
            alert(err)
            // console.log("onLoginPress -> err", err)
        })
    }

    // login JWT
    // const onLoginPress = () => {
    //     let data = {
    //         email: email,
    //         password: password
    //     }
    //     Axios.post(`${api}/login`, data, {
    //         timeout: 20000
    //     })
    //     .then((res)=>{
    //         // console.log("Login -> res", res)
    //         saveToken(res.data.token)
    //         navigation.navigate('Profile')
    //     })
    //     .catch((err)=>{
    //         console.log("Login -> err", err)
    //         ToastAndroid.show('Login failed.\nPlease check your username and password!\n\n'+err, ToastAndroid.SHORT)
    //     })
    // }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <View style={styles.loginPage}>
                <View style={styles.loginHeader}>
                    <Image source={require('../../assets/images/logo.jpg')} />
                </View>
                <View style={styles.loginBody}>
                    <Text style={styles.formLabel}>Username</Text>
                    <TextInput
                        value={email}
                        style={styles.textInput}
                        placeholder='Username or Email'
                        onChangeText={(email)=>{setEmail(email)}}
                    />
                    <Text style={styles.formLabel}>Password</Text>
                    <TextInput
                        value={password}
                        style={styles.textInput}
                        placeholder='Password'
                        secureTextEntry={true}
                        onChangeText={(password)=>{setPassword(password)}}
                    />
                    <Tombol bgColor={'#3ec6ff'} onPress={()=>onLoginPress()} title={"Login"} />
                    <View style={styles.hr}></View>
                    <Text style={styles.or}>OR</Text>
                    <GoogleSigninButton
                        onPress={() => signInWithGoogle()}
                        style={{width:'100%',height:48}}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Dark}
                    />
                    <Tombol bgColor={'#191970'} onPress={()=>signInWithFingerprint()} title={"Sign in with Fingerprint"} />
                </View>
                <View style={styles.loginFooter}>
                    <Text style={styles.formLabel}>Belum mempunyai akun ?</Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                        <Text style={{color:'#3598db'}}> Buat Akun</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Login;