import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Intro = () => {
	return (
		<View style={styles.container}>
			<Text>Hallo Kelas React Native Lanjutan Sanbercode!</Text>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	}
});

export default Intro;