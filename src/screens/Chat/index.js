import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

const Chat = () => {

	const [ messages, setMessages ] = useState([])
	const [ user, setUser ] = useState([])

	useEffect(() => {
		const user = auth().currentUser
		setUser(user)
		getData()
		return () => {
			const db = database().ref('messages')
			if (db) {
				db.off()
			}
		}
	}, [])

	const getData = () => {
		database().ref('messages').limitToLast(20).on('child_added', snapshot => {
			const value = snapshot.val()
			// console.log("getData -> value", value)
			setMessages(previousMessages => GiftedChat.append(previousMessages, value))
		})
	}

	const onSend = (( messages = [] ) => {
		// console.log("onSend -> messages", messages)
		for (let i = 0; i < messages.length; i++) {
			database().ref('messages').push({
				_id: messages[i]._id,
				createdAt: database.ServerValue.TIMESTAMP,
				text: messages[i].text,
				user: messages[i].user,

			})
		}
	})

	return (
		<GiftedChat
			messages={messages}
			onSend={messages => onSend(messages)}
			user={{
				_id: user.uid,
				name: user.email,
				avatar: "https://en.gravatar.com/userimage/6621161/0f56c6c0028f95d87823d03379d95062.jpeg"
			}}
		/>
	)
}

export default Chat;